import argparse
import os
import re

def main(dirpth,pattern,file_filter=".+py$"):
    for root,dirs,files in os.walk(dirpth):
        for file in files:
            if re.search(file_filter,file):
                fpth = os.path.join(root,file)
                with open(fpth) as f:
                    for line in f:
                        if re.search(pattern,file):
                            print'{}{}'.format(fpth,line),

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('dir')
    parser.add_argument('p')
    parser.add_argument('f')
    args=parser.parse_args()
    main(args.dir,args.p,args.f)